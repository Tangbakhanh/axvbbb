/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btap_tuan2;

/**
 *
 * @author sv
 */

public class Employee {
    private String Firstname;
    private String Lastname;
    private double Salary;
    public Employee (String Firstname,String Lastname,double Salary)
    {
        this.Firstname=Firstname;
        this.Lastname=Lastname;
        if(Salary>0)
        {
            this.Salary=Salary;
        }
        else
        {
            this.Salary=0.0;
        }
    }
    public  Employee(){
        
    }
    public double get_salary(int a,double salarypm){
        return a*12*salarypm;
    }
    public double getSalarypercent(int n,double salarypm,double percent)
    {
        return this.get_salary(n,salarypm) + this.get_salary(n,salarypm)*(percent/100);
    }
    
    public String getFirstname(){
        return this.Firstname;
    }
    
    public void setFirstname(String Firstname){
        this.Firstname=Firstname;
    }
    
    public String getLastname(){
        return this.Lastname;
    }
    
    public void setLastname(String Lastname){
        this.Lastname=Lastname;
    }
    
    public double getSalary(){
        return this.Salary;
    }
    
    public void setSalary(double Salary){
        this.Salary=Salary;
    }
    
    
}
